/**  Maxime Rayvich
 * 2032446
 */
public class BikeStore{
    public static void main(String[]args){
       Bicycle[] bikes = new Bicycle[4];
       bikes[0]=new Bicycle("bestManufacturer", 8, 40.5 );
       bikes[1]= new Bicycle("worstManufacturer", 10, 3);
       bikes[2]= new Bicycle("leastGoodestManufacturer", 3,5);
       bikes[3]= new Bicycle("meh",8,30); 

       for(int i = 0; i < bikes.length; i++){
           System.out.println(bikes[i]);
       }
    }

}
