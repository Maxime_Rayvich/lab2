/**  Maxime Rayvich
 * 2032446
 */
public class Bicycle{

    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manuChoice, int numGears, double spd){
        this.manufacturer = manuChoice;
        this.numberGears = numGears;
        this.maxSpeed = spd;

    }
    
    public String getManufacturer(){
        return this.manufacturer;
    }

    public  int getNumberGears(){
        return this.numberGears;

    }

    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public String toString(){
        return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + " , Maximum speed " + this.maxSpeed;

    }
}
